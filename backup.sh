#!/usr/bin/env bash

# set -x;

NO_COMPRESSION=0

pure_version() {
    echo '1.0.0'
}

version() {
    echo "$(basename $0) $(pure_version)"
}

usage() {
    version
    echo " --no-compress"
    echo "   exec backup without compression"
    echo " -b, -backupdir"
    echo "   set backup destination"
    echo " -k"
    echo "   create a dir YYYYMMDD_HHMMSS"
    echo " --version"
    echo "   show tool version number"
    echo " -h, --help"
    echo "   display this help"
    exit 0
}

while test "${1:0:1}" = "-"; do
    case "$1" in
        --no-compress)
            NO_COMPRESSION=1
            shift;
            ;;
        -b | --backupdir)
            _BACKUP_DIR="$2"
            shift; shift;
            ;;
        -k)
            _DATEDIR="$(date '+%Y-%m-%d_%H%M%S')"
            K=1;
            shift;
            ;;
        -h | --help)
            usage ;;
        --version)
            version
            exit 0 ;;
        -*)
            echo "Unknown option $1. Run with --help for help."
            exit 1 ;;
    esac
done

# start in folder script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit 1

if [ -z "$_BACKUP_DIR" ]; then
    OUT="$SCRIPT_DIR/backups"
elif [ ! -z "$_BACKUP_DIR" ] && [ -e "$_BACKUP_DIR" ]; then
    OUT="$_BACKUP_DIR"
else
    echo "⚠️ Backup dir doesn't exist..."
    exit 1
fi

if [[ $K -eq 1 ]]; then
    OUT="$OUT/$_DATEDIR"
fi

source libs/bklib.sh

# backup based on .ini files
# https://github.com/lra/mackup/tree/master/mackup/applications
if [[ ! -z "$(find cfgs -name *.cfg -type f -maxdepth 1)" ]]; then
    for INI in cfgs/*.cfg; do
        backup_ini "$INI" "$OUT" "$NO_COMPRESSION"
    done
else
    echo "⚠️    no cfgs given..."
fi
