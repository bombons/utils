#!/usr/bin/env bash

# https://stackoverflow.com/questions/12306223/how-to-manually-create-icns-files-using-iconutil
# fav: https://www.deviantart.com/hopstarter/gallery/

# set -x

scale_image() {
	echo "Creating $(basename "$2") $4..."
	sips -z $1 $1 "$2" --out "$3"/icon_$4.png > /dev/null
}

while [[ $1 = -?* ]]; do
  case $1 in
    -i|--icon)
      ICON="$2"
			shift
    ;;
    *)
      echo "invalid option: '$1'..."
      exit 1
    ;;
  esac
  shift
done

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# make icon set folder
ICON_SET="${SCRIPT_DIR}/$(basename "${ICON%.*}")".iconset
mkdir -p "$ICON_SET"

# create icon set
scale_image "16" "$ICON" "$ICON_SET" "16x16"
scale_image "32" "$ICON" "$ICON_SET" "16x16@2x"
scale_image "32" "$ICON" "$ICON_SET" "32x32"
scale_image "64" "$ICON" "$ICON_SET" "32x32@2x"
scale_image "128" "$ICON" "$ICON_SET" "128x128"
scale_image "256" "$ICON" "$ICON_SET" "128x128@2x"
scale_image "256" "$ICON" "$ICON_SET" "256x256"
scale_image "512" "$ICON" "$ICON_SET" "256x256@2x"
scale_image "512" "$ICON" "$ICON_SET" "512x512"
scale_image "1024" "$ICON" "$ICON_SET" "512x512@2x"

# create icns
echo "Converting iconset to $(basename "${ICON%.*}").icns..."
iconutil --convert icns "$ICON_SET"

# clean
rm -Rf "$ICON_SET"
