#!/usr/bin/env bash

# https://help.github.com/en/articles/syncing-a-fork

# set -x;

GIT=git
# GIT='echo # git'

function ck_empty() {
    if [[ "$1" == "" ]]; then
        return 1
    else
        return 0
    fi
}


founded=0

while [[ $founded -eq 0 ]]; do
    upstream="$(git remote -v | grep -E 'upstream' | tail -1)"
    ck_empty "$upstream"
    if [ $? -ne 0 ]; then
        QUESTION="paste original repo url"
        read -p "${QUESTION}: " ANS
        echo $ANS
        $GIT remote add upstream "$ANS"
        founded=1
    else
        founded=1
    fi
done

if [[ $founded -eq 1 ]]; then
    $GIT fetch upstream
    $GIT checkout master
    $GIT merge upstream/master
    $GIT push
fi
