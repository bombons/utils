#!/usr/bin/env bash

# https://stackoverflow.com/questions/238073/how-to-add-a-progress-bar-to-a-shell-script
# https://stackoverflow.com/questions/5799303/print-a-character-repeatedly-in-bash/17030976
# https://github.com/search?l=Shell&o=desc&p=2&q=progress+bar&ref=searchresults&s=stars&type=Repositories&utf8=%E2%9C%93

# set -x;

# start in folder script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit 1

source libs/rsynclib3b.sh

cpr "$@"
