#!/usr/bin/env bash

# set -x

check_dir()
{
  if [[ -e "$1" ]]; then
    return 0
  else
    return 1
  fi
}

clean() {
	local JUNK=('.DS_Store' '.AppleDouble' '.LSOverride')
	for JUNK_FILE in "${JUNK[@]}"; do
		find "$1" -type f -name "$JUNK_FILE" -exec rm -f {} \;
	done
}

compress()
{
	LOWER_NAME="$(basename "$1")"
    # echo $LOWER_NAME
    local GROUP="${LOWER_NAME##*-}"
    local NAME="$(echo "${LOWER_NAME%%.v*.*}" | tr '.' ' ')"
    # echo "$NAME | $GROUP"
	create_dmg "$NAME | $GROUP" "$1" "$2" "$LOWER_NAME"
}

create_dmg()
{
	echo "Converting to dmg $2..."
	hdiutil create -volname "$1" -srcfolder "$2" -ov -format $3 "$DIR/$4.dmg"
	echo
}

DIR=${!#} # last parameter
TO_COMPRESS=("${@:1:$#-1}") # folder to compress

if [[ ! -d "$DIR" ]]; then
  echo "$DIR is not a valid place..."
  exit 1
else
  echo "Creating dmg into $DIR..."
	echo
fi

for Z in "${TO_COMPRESS[@]}"; do
	check_dir "$Z"
	if [[ $? -ne 1 ]]; then
        ln -s "/Applications" "$Z"/Applications
		clean "$Z"
		compress "$Z" UDBZ
		# UDRW - UDIF read/write image
		# UDRO - UDIF read-only image
		# UDCO - UDIF ADC-compressed image
		# UDZO - UDIF zlib-compressed image
		# ULFO - UDIF lzfse-compressed image (OS X 10.11+ only)
		# UDBZ - UDIF bzip2-compressed image (Mac OS X 10.4+ only)
		# UDTO - DVD/CD-R master for export
		# UDSP - SPARSE (grows with content)
		# UDSB - SPARSEBUNDLE (grows with content; bundle-backed)
	fi
done
