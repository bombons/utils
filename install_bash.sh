#!/usr/bin/env bash

# set -x;

RUBY=/usr/bin/ruby
SUDO=sudo
BREW=brew
CHSH=chsh
# RUBY='echo # ruby'
# SUDO='echo # sudo'
# BREW='echo # brew'
# CHSH='echo # chsh'

# start in folder script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR" || exit 1

source libs/utils.sh

if [[ ${BASH_VERSINFO[0]} -ge 3 ]]
then

    if [[ $(is_installed brew) == 0 ]]
    then
        $RUBY -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi

    $BREW install bash

    warn_about_superuser

    if [[ $(cat /etc/shells) != */usr/local/bin/bash* ]]
    then
        echo /usr/local/bin/bash | $SUDO tee -a /etc/shells
    fi

    $CHSH -s /usr/local/bin/bash
    $SUDO chsh -s /usr/local/bin/bash

fi

# eof
