#!/usr/bin/env bash
# $1 name
# $2 size in MB

DISK_NAME="$1"
SIZE="$2"
DISK_SIZE="$((SIZE*2048))"
diskutil erasevolume HFS+ "${DISK_NAME}" `hdiutil attach -nomount ram://${DISK_SIZE}`
