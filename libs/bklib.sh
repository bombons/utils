#!/usr/bin/env bash

CPP='cpr'
MKDIR='mkdir'
# CPP='echo #cpp'
# MKDIR='echo # mkdir'

source "$(dirname ${BASH_SOURCE[0]})"/libcompress.sh
source "$(dirname ${BASH_SOURCE[0]})"/utils.sh
source "$(dirname ${BASH_SOURCE[0]})"/rsynclib3b.sh
# source "$(dirname ${BASH_SOURCE[0]})"/cplib.sh

activate_colors

EXCEPTIONS="desktop|documents|downloads|drive|dropbox|movies|music|pictures"

backup_ini()
{
    # load the ini file
    source "$1"

    # load name section
    check_empty "$app"
    if [ $? -ne 0 ]; then
        echo "$BOLD$BLUE==> $WHITE$app...$RESET"

        # backup date
        _DATE=$( date +%Y%m%d_%H%M%S )
        # create backup dir
        BACKUP_DIR="$2/$app--$_DATE"
        mkdir -p "$(dirname "$BACKUP_DIR")" "$BACKUP_DIR"

        # load files to copy
        # assumed empty filelist
        exist=0
        check_empty "${files[*]}"
        if [ $? -ne 0 ]; then
            for X in "${files[@]}"; do
                # check existence and copy
                check "$X"
                if [ $? -ne 1 ]
                then
                    $CPP "$X" "$BACKUP_DIR"
                    exist=1
                fi
            done
        fi

        # exec custom commands
        check_empty "${backup[*]}"
        if [ $? -ne 0 ]; then
            local OLD_PATH="$PWD"
            cd "$BACKUP_DIR" || exit 1
            for Y in "${backup[@]}"; do
                echo "exec $Y"
                eval "$Y"
                exist=1
            done
            cd "$OLD_PATH" || exit 1
        fi

        # skip every time copression on folder
        if [[ "$EXCEPTIONS" == "" || "`echo "$(basename "$1")" | grep -vE "$EXCEPTIONS"`" != "" ]]; then
            # start compress of others
            if [[ $3 -ne 1 ]]; then
                if [ $exist -eq 1 ]; then
                    local OLD_PATH="$PWD"
                    clean "$BACKUP_DIR"
                    cd "$BACKUP_DIR" || exit 1
                    compress_zip "$BACKUP_DIR" '*'
                    cd "$OLD_PATH" || exit 1
                    rm -Rf "$BACKUP_DIR"
                    du -h "$BACKUP_DIR".*
                else
                    echo "$YELLOW[!]$RESET Backup folder is empty, rm it..."
                    rm -Rf "$BACKUP_DIR"
                fi
            fi
        else
            if [ ! "$(ls -A "$BACKUP_DIR")" ]; then
                rm -Rf "$BACKUP_DIR"
            fi
        fi

    fi
}

restore_ini()
{
    # load the ini file
    source "$1"

    # load name section
    check_empty "$app"
    if [ $? -ne 0 ]; then
        echo "$BOLD$BLUE==> $WHITE$app...$RESET"

        if [[ "$EXCEPTIONS" == "" || "`echo "$(basename "$1")" | grep -vE "$EXCEPTIONS"`" != "" ]]; then
            # find last backup avaiable
            local LAST_BACKUP="$(find "$2" -type f -maxdepth 1 -name "$app--*.zip" | sort -r | head -1)"
        else
            # find last backup avaiable
            local LAST_BACKUP="$(find "$2" -type d -maxdepth 1 -name "$app--*" | sort -r | head -1)"
        fi

        # check if a backup is found
        check_empty "${LAST_BACKUP}"
        if [ $? -ne 0 ]; then
            printf "%s\n" "I'll restore from $(basename ${LAST_BACKUP})..."

            # remove extension
            local BACKUP_NAME="${LAST_BACKUP%*.zip}"

            if [[ "$EXCEPTIONS" == "" || "`echo "$(basename "$1")" | grep -vE "$EXCEPTIONS"`" != "" ]]; then
                # decompress it
                if [[ -e "$BACKUP_NAME" ]]; then
                    rm -Rf "$BACKUP_NAME"
                fi
                extract_zip "$LAST_BACKUP"
            fi

            # copy files
            # assumed empty filelist
            exist=0
            if [[ "$EXCEPTIONS" == "" || "`echo "$(basename "$1")" | grep -vE "$EXCEPTIONS"`" != "" ]]; then

              check_empty "${files[*]}"
              if [ $? -ne 0 ]; then
                  for X in "${files[@]}"; do

                      # extract dirname and file to copy
                      local _DIR="$(dirname "$X")"
                      local _FILE="$(basename "$X")"

                      # check existence of the file and copy it
                      check "$BACKUP_NAME/$_FILE"
                      if [ $? -ne 1 ]; then
                          check "$_DIR"
                          if [ $? -ne 0 ]; then
                              $MKDIR -p "$_DIR"
                          fi
                          $CPP "$BACKUP_NAME/$_FILE" "$_DIR"
                      fi
                  done
              fi

            else
                check_empty "${BACKUP_NAME}"
                if [ $? -ne 0 ]; then
                    # extract dirname and file to copy
                    local _DIR="$(dirname "${files[*]}")"
                    # check existence of the file and copy it

                    check "$_DIR"
                    if [ $? -ne 0 ]; then
                        $MKDIR -p "$_DIR"
                    fi
                    $CPP "$BACKUP_NAME"/* "$_DIR"

                fi
            fi

            # exec custom commands
            check_empty "${restore[*]}"
            if [ $? -ne 0 ]; then
                local OLD_PATH="$PWD"
                cd "$BACKUP_NAME" || exit 1
                for Y in "${restore[@]}"; do
                    echo "exec $Y"
                    eval "$Y"
                done
                cd "$OLD_PATH" || exit 1
            fi

        else
            echo "$YELLOW[!]$RESET No backup for $app found..."
        fi

    fi
}
