#!/usr/bin/env bash

# https://stackoverflow.com/questions/12043965/parsing-rsync-output
# https://stackoverflow.com/questions/16319179/realtime-removal-of-carriage-return-in-shell
# https://stackoverflow.com/questions/7022390/how-to-read-the-second-to-last-line-in-a-file-using-bash
# /https://stackoverflow.com/questions/60437368/rsync-suppress-sigint-for-trap-outside


# set -x;

RSYNC='rsync -ah --info=progress2 --no-i-r'
# RSYNC='echo #rsync -ah --info=progress2 --no-i-r'

TMP_DIR=/tmp/com.bombons.cpr
FILE_LIST=$TMP_DIR/list.txt
mkdir -p $TMP_DIR && rm -Rf $TMP_DIR/*

source "$(dirname ${BASH_SOURCE[0]})"/utils.sh

# check rsync version
CURVERSION=$(rsync --version | sed -n '1s/^rsync *version \([0-9.]*\).*$/\1/p')
REQVERSION="3.1.0"
if ! [ "$(printf '%s\n' "$REQVERSION" "$CURVERSION" | sort -V | head -n1)" = "$REQVERSION" ]; then
    echo "Less than $REQVERSION"
    echo "Need to install newer version of rsync"
    echo "  brew install rsync"
    exit 1
fi

_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

gen_bar() {
  local str=$1
  local num=$2
  local v="$(printf "%-${num}s" "$str")"
  echo "${v// /$str}"
}

progress_bar()
{
  local p="$1"

  local tcols="$(tput cols)"
  local ncols=$(($tcols-${#p}-3))

  local strdone="$(gen_bar "#" $ncols)"
  local strtodo="$(gen_bar ' ' $ncols)"

  local lendone=$(($p * ${#strdone} / 100))
  let lentodo=${#strdone}-$lendone

  printf '%s\r' "${strdone:0:$lendone}${strtodo:0:$lentodo} $p%"
  # gecho -ne "[${strdone:0:$lendone}${strtodo:0:$lentodo}] $p%\r"

}

cp_echo()
{
  # echo "cp $( basename "$1" ) → $( dirname "$2" )..."
  echo "cp $( basename "$1" ) to $( dirname "$2" )..."
  # echo "cp $( basename "$1" )..."
}

# function spinner {
#   case "$1" in
#     start)
#         local old_lines=-1
#         while true; do
#             tag=$( tail -2 $FILE_LIST | head -1 | grep -o "[0-9]*%" | tr -d '%' )
#
#             local current_lines=$(cat $FILE_LIST | wc -l)
#             local current_lines=$(($current_lines+0))
#
#             if [[ -z "$tag" ]]; then
#                 progress_bar 0
#             elif [[ "$tag" -gt 100 ]]; then
#                 progress_bar 0
#             else
#                 progress_bar "$tag"
#                 if [ $tag == 100 ]; then
#                     echo ""
#                     break;
#                 fi
#             fi
#
#             sleep .1
#
#             # if [[ $current_lines -eq $old_lines ]]; then
#             #     echo ""
#             #     # break;
#             # fi
#             local old_lines=$current_lines
#
#         done
#         ;;
#     stop)
#         # tag=$( tail -2 $FILE_LIST | head -1 | grep -o "[0-9]*%" | tr -d '%' )
#         # if [[ -z "$tag" ]]; then
#         #     progress_bar 0
#         # elif [[ "$tag" -gt 100 ]]; then
#         #     progress_bar 100
#         # elif [[ "$tag" -eq 0 ]]; then
#         #     progress_bar 100
#         # else
#         #     progress_bar "$tag"
#         # fi
#         # progress_bar 100
#         # echo ""
#         exec 2>/dev/null
#         kill $2
#     ;;
#   esac
# }

function spinner {
  case "$1" in
    start)
      local WAIT=0.1
      local old_lines=-1
      local current_lines=0
      local interrupted=0

      while [[ $interrupted -eq 0 ]]; do
        tag=$( tail -2 $FILE_LIST | head -1 | grep -o "[0-9]*%" | tr -d '%' )
        # local current_lines=$(cat $FILE_LIST | wc -l)
        # local current_lines=$(($current_lines+0))

        if [[ -z "$tag" ]]; then
            progress_bar 0
        elif [[ "$tag" -gt 100 ]]; then
            progress_bar 0
        else
            progress_bar "$tag"
            if [ $tag == 100 ]; then
                interrupted=1
            fi
        fi

        if [[ $current_lines -eq $old_lines ]]; then
            interrupted=1
        fi

        local old_lines=$current_lines
        sleep $WAIT

      done
    ;;
    stop)
        # exec 2>/dev/null
        kill $2 > /dev/null 2>&1
    ;;
  esac
}

# function spinner {
#   case "$1" in
#     start)
#     local SPIN=( "[ | ]" "[ \ ]" "[ - ]" "[ / ]" )
#     local WAIT=0.5
#     local old_lines=-1
#     local current_lines=0
#     local interrupted=0
#         while [[ $interrupted -eq 0 ]]; do
#             local tag=$( tail -2 $FILE_LIST | head -1 | grep -o "[0-9]*%" | tr -d '%' )
#             local current_lines=$(cat $FILE_LIST | wc -l)
#             local current_lines=$(($current_lines+0))
#
#             for INDEX in "${SPIN[@]}"; do
#                 printf '%s %s\r' "$INDEX" "$tag"
#
#                 if [[ "$tag" == 100 ]]; then
#                     interrupted=1
#                 fi
#
#                 if [[ $current_lines -eq $old_lines ]]; then
#                     interrupted=1
#                 fi
#
#                 local old_lines=$current_lines
#                 sleep $WAIT
#             done
#         done
#     ;;
#     stop)
#         exec 2>/dev/null
#         kill $2
#     ;;
#   esac
# }

cpr()
{

    local SRC=("${@:1:$#-1}")
    local DST="${@: -1}"

    local i=1
    # copy
    for src in "${SRC[@]}"
    do
        rm -Rf $FILE_LIST
        # convert to complete path
        src="$(_realpath "$src")"

        cp_echo "$src" "$DST"

        # ###############################################
        # set +m
        #
        # spinner start & local PID=$!
        # trap "exit 0" INT TERM EXIT
        # $RSYNC "$src" "$DST" | awk '1;{fflush()}' RS='\r\n' > $FILE_LIST
        # wait $PID
        # spinner stop $PID
        # set -m
        # trap - SIGINT SIGTERM SIGQUIT
        # echo "--- giro $i ---"
        # ###############################################
        # i=$(($i+1))

        # set +m
        spinner start & PID=$!
        trap "spinner stop $PID; exit" INT TERM EXIT
        $RSYNC "$src" "$DST" | awk '1;{fflush()}' RS='\r\n' > $FILE_LIST
        wait $PID
        echo
        # set -m

    done

}

# eof
