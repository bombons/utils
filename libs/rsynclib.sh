#!/usr/bin/env bash

# https://unix.stackexchange.com/questions/245801/how-do-i-reference-all-files-including-hidden-files

# set -x;

RSYNC='rsync -azh --info=progress2 --no-i-r'
# RSYNC='echo #rsync -azh --info=progress2'

source "$(dirname ${BASH_SOURCE[0]})"/utils.sh

# check rsync version
CURVERSION=$(rsync --version | sed -n '1s/^rsync *version \([0-9.]*\).*$/\1/p')
REQVERSION="3.1.0"
if ! [ "$(printf '%s\n' "$REQVERSION" "$CURVERSION" | sort -V | head -n1)" = "$REQVERSION" ]; then
    echo "Less than $REQVERSION"
    echo "Need to install newer version of rsync"
    echo "  brew install rsync"
    exit 1
fi

_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

cp_echo()
{
  echo "copying $( basename "$1" )..."
  # echo "copying $( basename "$1" ) → $( dirname "$2" )..."
  # echo "copying $( basename "$1" ) to $( dirname "$2" )..."
  # echo "copying $( basename "$1" )..."
}

cpr()
{

    local SRC=("${@:1:$#-1}")
    local DST="${@: -1}"

    # copy
    for src in "${SRC[@]}"
    do
        # convert to complete path
        src="$(_realpath "$src")"

        cp_echo "$src"
        $RSYNC "$src" "$DST"

    done

    # unset cpp_rename

}

# eof
