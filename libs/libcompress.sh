#!/usr/bin/env bash

# set -x;

compress_zip()
# $1 archive name
# $2 folder to compress
{
    echo "compressing $(basename "$1")..."
    # zip -X only for macos
    zip -9 -r -q -X "$1.zip" $2
    # unzip archive_name.zip
}

store_zip()
# $1 archive name
# $2 folder to compress
{
    echo "compressing $(basename "$1")..."
    # zip -X only for macos
    zip -1 -r -q -X "$1.zip" $2
    # unzip archive_name.zip
}

extract_zip()
{
    echo "extracting $(basename "$1")..."
    unzip -q -d "${1%*.zip}" "$1"
}

compress_bz2()
# $1 archive name
# $2 folder to compress
{
    echo "compressing $(basename "$1")..."
    tar -cf - $2 | bzip2 -9 -v - > "$1".tar.bz2
}

compress_xz()
# $1 archive name
# $2 folder to compress
{
    echo "compressing $(basename "$1")..."
    tar -cf - $2 | xz -9 -T0 -v - > "$1".tar.xz
}

clean()
{
    local JUNK=('.DS_Store' '.AppleDouble' '.LSOverride')
    for JUNK_FILE in "${JUNK[@]}"; do
        find "$1" -type f -name "$JUNK_FILE" -exec rm -f {} \;
    done
}
