#!/usr/bin/env bash

# https://unix.stackexchange.com/questions/245801/how-do-i-reference-all-files-including-hidden-files

# set -x;

CP='cp -a'
# CP='echo #cp -a'
MKDIR='mkdir -p'
# MKDIR='echo #mkdir'

_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

gen_bar() {
  local str=$1
  local num=$2
  local v="$(printf "%-${num}s" "$str")"
  echo "${v// /$str}"
}

cp_echo()
{
  echo "copying $( basename "$1" )..."
  # echo "copying $( basename "$1" ) → $( dirname "$2" )..."
  # echo "copying $( basename "$1" ) to $( dirname "$2" )..."
  # echo "copying $( basename "$1" )..."
}

progress_bar()
{
  p="$1"

  tcols="$(tput cols)"
  ncols=$(($tcols-${#p}-3))

  strdone="$(gen_bar "#" $ncols)"
  strtodo="$(gen_bar ' ' $ncols)"

  lendone=$(($p * ${#strdone} / 100))
  let lentodo=${#strdone}-$lendone

  printf '%s\r' "${strdone:0:$lendone}${strtodo:0:$lentodo} $p%"
  # gecho -ne "[${strdone:0:$lendone}${strtodo:0:$lentodo}] $p%\r"

}

cp_bar()
{
  $CP "$1" "$2" &

  # src_size=$(stat -c%s "$1") #linux
  src_size=$(stat -f%z "$1")
  # src_size=$(find "$1" | xargs stat -f%z | awk '{ s+=$1 } END { print s }')

  if [[ $src_size -ne 0 ]]; then
    while true; do
        # tgr_size=$(stat -c%s "$2")       # how much work is done so far, linux
        tgr_size=$(stat -f%z "$2")       # how much work is done so far
        # tgr_size=$(find "$2" | xargs stat -f%z | awk '{ s+=$1 } END { print s }')       # how much work is done so far

        p=$(( $tgr_size * 100 / $src_size ))

        progress_bar "$p"

        if [ $tgr_size == $src_size ]; then
            echo ""                        # add a new line at the end
            break;
        fi

        sleep .1
    done
  else
    progress_bar 100
    echo ""
  fi

}

cpp_rename()
{
    local SRC="$1"
    local DST="$2"
    local DSTDIR="$( dirname "$DST" )"

    # checks minimum args
    if [ $# -ne 2 ]
    then
        echo "too few args"
        return 1
    fi

    # check if source file exists
    if ! [ -e "$SRC" ]
    then
        echo "$SRC doesn't exist"
        return 1
    fi

    # check if source file is a directory
    if [ -d "$SRC" ]
    then
        echo "[!] You cannot make a copy of $SRC to $DST..."
        echo "$SRC is a directory and $DST is a file."
        return 1
    fi

    if ! [ -d "$DSTDIR" ]
    then
        echo "$DSTDIR does not exist"
        return 1
    fi

    cp_echo "$SRC" "$DST"
    cp_bar "$SRC" "$DST"
}

cpp()
{

    local SRC=("${@:1:$#-1}")
    local DST="${@: -1}"

    # checks minimum args
    if [ $# -lt 2  ]
    then
        echo "too few args"
        return 1
    fi

    # use as a renamer, works only with two args and
    if [ ! -d "$DST" ]
    then
        cpp_rename "$@"
        return $?
    fi

    # copy
    for src in "${SRC[@]}"
    do
        # convert to complete path
        src="$(_realpath "$src")"

        # not a dir
        if [[ ! -d "$src" ]]
        then
            local dst="$DST/$( basename "$src" )"
            cp_echo "$src" "$dst"
            cp_bar "$src" "$dst"
        # is a dir
        elif [[ -d "$src" ]]
        then
            local dir="$DST/$( basename "$src" )"
            $MKDIR -p "$dir" || continue

            if [[ "$(du -sk "$src" | cut -f1)" -ne 0 ]]
            then
                shopt -s dotglob # enable dotglob
                local srcs=( "$src"/* )
                shopt -u dotglob # disable dotglob again

                cpp "${srcs[@]}" "$dir"
            fi
        else
            echo "[!] error, not expected..."
            exit 0
        fi

    done

    # unset cpp_rename

}

# eof
