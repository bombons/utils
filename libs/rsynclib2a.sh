#!/usr/bin/env bash

# https://stackoverflow.com/questions/12043965/parsing-rsync-output
# https://stackoverflow.com/questions/16319179/realtime-removal-of-carriage-return-in-shell
# https://stackoverflow.com/questions/7022390/how-to-read-the-second-to-last-line-in-a-file-using-bash

# set -x;

RSYNC='rsync -ah --info=progress2 --no-i-r'
# RSYNC='echo #rsync -ah --info=progress2 --no-i-r'

TMP_DIR=/tmp/com.bombons.cpr
FILE_LIST=$TMP_DIR/list.txt
mkdir -p $TMP_DIR

source "$(dirname ${BASH_SOURCE[0]})"/utils.sh

# check rsync version
CURVERSION=$(rsync --version | sed -n '1s/^rsync *version \([0-9.]*\).*$/\1/p')
REQVERSION="3.1.0"
if ! [ "$(printf '%s\n' "$REQVERSION" "$CURVERSION" | sort -V | head -n1)" = "$REQVERSION" ]; then
    echo "Less than $REQVERSION"
    echo "Need to install newer version of rsync"
    echo "  brew install rsync"
    exit 1
fi

_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

gen_bar() {
  local str=$1
  local num=$2
  local v="$(printf "%-${num}s" "$str")"
  echo "${v// /$str}"
}

progress_bar()
{
  local p="$1"

  local tcols="$(tput cols)"
  local ncols=$(($tcols-${#p}-3))

  local strdone="$(gen_bar "#" $ncols)"
  local strtodo="$(gen_bar ' ' $ncols)"

  local lendone=$(($p * ${#strdone} / 100))
  let lentodo=${#strdone}-$lendone

  printf '%s\r' "${strdone:0:$lendone}${strtodo:0:$lentodo} $p%"
  # gecho -ne "[${strdone:0:$lendone}${strtodo:0:$lentodo}] $p%\r"

}

cp_echo()
{
  # echo "cp $( basename "$1" ) → $( dirname "$2" )..."
  echo "cp $( basename "$1" ) to $( dirname "$2" )..."
  # echo "cp $( basename "$1" )..."
}

cpr()
{

    local SRC=("${@:1:$#-1}")
    local DST="${@: -1}"

    # copy
    for src in "${SRC[@]}"
    do
        interrupted=0
        # convert to complete path
        src="$(_realpath "$src")"

        cp_echo "$src" "$DST"
        $RSYNC "$src" "$DST" | awk '1;{fflush()}' RS='\r\n' > $FILE_LIST &
        while [[ $interrupted -eq 0 ]]; do

            tag=$( tail -2 $FILE_LIST | head -1 | grep -o "[0-9]*%" | tr -d '%' )
            local current_lines=$(cat $FILE_LIST | wc -l)
            local current_lines=$(($current_lines+0))

            if [[ -z "$tag" ]]; then
                progress_bar 0
            elif [[ "$tag" -gt 100 ]]; then
                progress_bar 0
            else
                progress_bar "$tag"
                if [ $tag == 100 ]; then
                    echo ""
                    interrupted=1
                    break
                fi
            fi

            if [[ $current_lines -eq $old_lines ]]; then
                echo ""
                interrupted=1
                break
            fi

            local old_lines=$current_lines

            sleep .3
        done

    done

}

# eof
