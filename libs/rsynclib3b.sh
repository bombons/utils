#!/usr/bin/env bash

# https://stackoverflow.com/questions/12043965/parsing-rsync-output
# https://stackoverflow.com/questions/16319179/realtime-removal-of-carriage-return-in-shell
# https://stackoverflow.com/questions/7022390/how-to-read-the-second-to-last-line-in-a-file-using-bash
# /https://stackoverflow.com/questions/60437368/rsync-suppress-sigint-for-trap-outside
# https://www.google.com/search?q=stdbuf+rsync&client=firefox-b-e&ei=uOmfXoKpK9GvrgSF3a3gBg&start=10&sa=N&ved=2ahUKEwiC5bqgufvoAhXRl4sKHYVuC2wQ8NMDegQICxBC&biw=769&bih=1176&dpr=2.22
# https://bbs.archlinux.org/viewtopic.php?id=206822
# https://stackoverflow.com/questions/11454343/pipe-output-to-bash-function/11454477

# set -x;

RSYNC='rsync -ah --info=progress2,stats --no-i-r'
# RSYNC='echo #rsync -ah --info=progress2 --no-i-r'

source "$(dirname ${BASH_SOURCE[0]})"/utils.sh

# check rsync version
CURVERSION=$(rsync --version | sed -n '1s/^rsync *version \([0-9.]*\).*$/\1/p')
REQVERSION="3.1.0"
if ! [ "$(printf '%s\n' "$REQVERSION" "$CURVERSION" | sort -V | head -n1)" = "$REQVERSION" ]; then
    echo "Less than $REQVERSION"
    echo "Need to install newer version of rsync"
    echo "  brew install rsync"
    exit 1
fi

_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

gen_bar() {
  local str=$1
  local num=$2
  local v="$(printf "%-${num}s" "$str")"
  echo "${v// /$str}"
}

progress_bar()
{
  local p=$(echo "$1" | tr -d '%')

  local tcols="$(tput cols)"
  local ncols=$(($tcols-${#p}-3))

  local strdone="$(gen_bar "#" $ncols)"
  local strtodo="$(gen_bar ' ' $ncols)"

  local lendone=$(($p * ${#strdone} / 100))
  let lentodo=${#strdone}-$lendone

  printf '%s\r' "${strdone:0:$lendone}${strtodo:0:$lentodo} $p%"
  # gecho -ne "[${strdone:0:$lendone}${strtodo:0:$lentodo}] $p%\r"

}

cp_echo()
{
  # echo "cp $( basename "$1" ) → $( dirname "$2" )..."
  echo "cp $( basename "$1" ) to $2..."
  # echo "cp $( basename "$1" )..."
}

foo()
{
    while read data; do
        progress_bar "$data"
    done
}

cpr()
{

    local SRC=("${@:1:$#-1}")
    local DST="${@: -1}"

    local i=1

    # copy
    for src in "${SRC[@]}"
    do
        rm -Rf $FILE_LIST
        # convert to complete path
        src="$(_realpath "$src")"

        cp_echo "$src" "$DST"

        $RSYNC "$src" "$DST" | stdbuf -oL awk 'BEGIN { RS="\r" } /%/ { print $2 }' | foo & wait
        echo

    done

}

# eof
