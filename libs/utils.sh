#!/usr/bin/env bash

# set -x

# return 1 if installed
is_installed()
{
    # set to 1 initially
    local return_=1
    # set to 0 if not found
    type $1 >/dev/null 2>&1 || { local return_=0; }
    # return value
    echo "$return_"
}

warn_about_superuser()
{
    if [ "$(id -u)" != "0" ]; then
        sudo -p "This script requires superuser access... " printf "%s\r" ""
    fi
}

activate_colors()
{
    #COLOR & FORMATTING
    BLACK=$(tput setaf 0)
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    YELLOW=$(tput setaf 3)
    BLUE=$(tput setaf 4)
    MAGENTA=$(tput setaf 5)
    CYAN=$(tput setaf 6)
    WHITE=$(tput setaf 7)
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
    UNDERLINE=$(tput smul)
    RMUNDERLINE=$(tput rmul)
}

check_empty()
{
    for x in $1; do
        if [ -z "$x" ]; then
            return 0
        else
            return 1
        fi
    done
}

check()
{
    for x in "$1"; do
        if [[ -e "$x" ]]; then
            return 0
        else
            return 1
        fi
    done
}


































# eof
