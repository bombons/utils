#!/usr/bin/env bash

### TERMINAL ###
echo "" >> ~/.bash_profile
echo "export PS1='\[\e[1;37m\]\u@\h:\[\e[1;34m\]\w \$ \[\e[0m\]'" >> ~/.bash_profile
echo "export CLICOLOR=1" >> ~/.bash_profile
echo "export LSCOLORS=GxFxCxDxBxegedabagaced" >> ~/.bash_profile

if [[ "$1" != "" ]]; then
  sudo scutil --set HostName "$1"
fi
